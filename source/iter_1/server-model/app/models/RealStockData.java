package models;

import play.*;
import play.db.jpa.*;

import javax.persistence.*;
import java.util.*;
/*
 * AfterHoursChangeRealtime : N/A - N/A
AnnualizedGain : None
Ask : 30.60
AskRealtime : 30.60
AverageDailyVolume : 2621020
Bid : 30.55
BidRealtime : 30.55
BookValue : 23.762
Change : -0.35
ChangeFromFiftydayMovingAverage : +0.304
ChangeFromTwoHundreddayMovingAverage : +2.3669
ChangeFromYearHigh : -0.80
ChangeFromYearLow : +6.25
ChangePercentRealtime : N/A - -1.13%
ChangeRealtime : -0.35
Change_PercentChange : -0.35 - -1.13%
ChangeinPercent : -1.13%
Commission : None
DaysHigh : 30.75
DaysLow : 30.45
DaysRange : 30.45 - 30.75
DaysRangeRealtime : N/A - N/A
DaysValueChange : - - -1.13%
DaysValueChangeRealtime : N/A - N/A
DividendPayDate : None
DividendShare : 0.76
DividendYield : 2.46
EBITDA : 16.912B
EPSEstimateCurrentYear : 0.00
EPSEstimateNextQuarter : 0.00
EPSEstimateNextYear : 0.00
EarningsShare : 2.55
ErrorIndicationreturnedforsymbolchangedinvalid : None
ExDividendDate : Aug 24
FiftydayMovingAverage : 30.246
HighLimit : None
HoldingsGain : None
HoldingsGainPercent : - - -
HoldingsGainPercentRealtime : N/A - N/A
HoldingsGainRealtime : None
HoldingsValue : None
HoldingsValueRealtime : None
LastTradeDate : 12/17/2012
LastTradePriceOnly : 30.55
LastTradeRealtimeWithTime : N/A - <b>30.55</b>
LastTradeTime : 2:02am
LastTradeWithTime : 2:02am - <b>30.55</b>
LowLimit : None
MarketCapRealtime : None
MarketCapitalization : 176.9B
MoreInfo : cnprIed
Name : MTR CORPORATION
Notes : None
OneyrTargetPrice : 29.80
Open : 30.65
OrderBookRealtime : None
PEGRatio : None
PERatio : 12.12
PERatioRealtime : None
PercebtChangeFromYearHigh : -2.55%
PercentChange : -1.13%
PercentChangeFromFiftydayMovingAverage : +1.01%
PercentChangeFromTwoHundreddayMovingAverage : +8.40%
PercentChangeFromYearLow : +25.72%
PreviousClose : 30.90
PriceBook : 1.30
PriceEPSEstimateCurrentYear : None
PriceEPSEstimateNextYear : None
PricePaid : None
PriceSales : 5.20
SharesOwned : None
ShortRatio : None
StockExchange : HKSE
Symbol : 0066.HK
TickerTrend :  ==-=== 
TradeDate : None
TwoHundreddayMovingAverage : 28.1831
Volume : 1726256
YearHigh : 31.35
YearLow : 24.30
YearRange : 24.30 - 31.35
symbol : 0066.HK
[Finished in 0.2s]
 */
@Entity
public class RealStockData  extends Model{
	public String AfterHoursChangeRealtime; // N-A - N/A
	public Float AnnualizedGain;
	public Float Ask;
	public Float AskRealtime;
	public Float AverageDailyVolume;
	public Float Bid;
	public Float BidRealtime;
	public Float BookValue;
	//public String Change; replaced by changeamount
	//public String Change_PercentChange; replaced by changereate
	public Float ChangeFromFiftydayMovingAverage;
	public Float ChangeFromTwoHundreddayMovingAverage;
	public Float ChangeFromYearHigh;
	public Float ChangeFromYearLow;
	public Float ChangeinPercent;
	public String ChangePercentRealtime; // N/A - -1.13%
	public Float ChangeRealtime;
	public Float Commission;
	//public String DaysHigh; replaced by dayhigh
	//public String DaysLow; replaced by daylow
	public String DaysRange; //30.45 - 30.75
	public String DaysRangeRealtime;// N/A - N/A
	public String DaysValueChange;// - - -1.13%
	public String DaysValueChangeRealtime;//N/A - N/A
	public Float DividendPayDate;
	public Float DividendShare;
	public Float DividendYield;
	public Float EarningsShare;
	public String EBITDA;// 16.912B
	public Float EPSEstimateCurrentYear;
	public Float EPSEstimateNextQuarter;
	public Float EPSEstimateNextYear;
	public String ErrorIndicationreturnedforsymbolchangedinvalid;// Null
	public String ExDividendDate;//Aug 24
	public Float FiftydayMovingAverage;
	public Float HighLimit;
	public Float HoldingsGain;
	public String HoldingsGainPercent; // - - -
	public String HoldingsGainPercentRealtime;
	public String HoldingsGainRealtime;
	public Float HoldingsValue;
	public String HoldingsValueRealtime;
	public String LastTradeDate;
	public String LastTradePriceOnly;
	public String LastTradeRealtimeWithTime;
	public String LastTradeTime;
	public String LastTradeWithTime;
	public String LowLimit;
	public String MarketCapitalization;
	public String MarketCapRealtime;
	public String MoreInfo;
	//public String Symbol; replaced by stock.name
	public String stockName;
	public String Notes;
	public Float OneyrTargetPrice;
	public String OrderBookRealtime;
	public String PEGRatio;
	public Float PERatio;
	public String PERatioRealtime;
	public Float PercentChangeFromYearHigh; // %
	public Float PercentChange;
	public Float PercentChangeFromFiftydayMovingAverage;
	public Float PercentChangeFromTwoHundreddayMovingAverage;
	public Float PercentChangeFromYearLow;
	public Float PreviousClose;
	public Float PriceBook;
	//public String Open; replaced by openprice
	public Float PriceEPSEstimateCurrentYear;
	public Float PriceEPSEstimateNextYear;
	public Float PricePaid;
	public Float PriceSales;
	public String SharesOwned;
	public String ShortRatio;
	public String StockExchange;
	public String symbol;
	public String TickerTrend;
	public String TradeDate;
	public Float TwoHundreddayMovingAverage;
	//public String Volume; replaced by volume
	public Float YearHigh;
	public Float YearLow;
	public String YearRange;
	
	public Float changerate;
    public Float changeamount;
    public Float tradingvolume;
    public Float daylow;
    public Float dayhigh;
    public Float openprice;
    public Float prevclose;
    
    public Date time;
}
