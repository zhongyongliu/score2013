package models;

import play.*;
import play.data.validation.Required;
import play.db.jpa.*;

import javax.persistence.*;


import java.util.*;

@Entity
public class NewsKeyword extends Model {
	
	@Required
	@Column(nullable = false)
    public String keyword;
	
    @ManyToMany(mappedBy = "keywords")
    public List<News> relatedNews;
}
