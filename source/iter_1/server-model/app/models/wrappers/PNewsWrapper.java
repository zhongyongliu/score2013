package models.wrappers;

import java.util.Date;

public interface PNewsWrapper {

	public abstract String getAbst();

	public abstract Date getTime();

	public abstract Long getId();


}