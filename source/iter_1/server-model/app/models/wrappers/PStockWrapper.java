package models.wrappers;

import java.util.List;

public interface PStockWrapper {

	public abstract String getMarket();

	public abstract String getName();

	public abstract String getParcode();

	public abstract String getFullcode();

	public abstract Float getChangerate();

	public abstract Float getChangeamount();

	public abstract Float getTradingvolume();

	public abstract Float getDaylow();

	public abstract Float getDayhigh();

	public abstract Float getOpenprice();

	public abstract Float getPrevclose();

	public abstract List<Long> getNewsids();

	//delete detail getter
}