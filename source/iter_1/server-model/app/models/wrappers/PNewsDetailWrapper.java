package models.wrappers;

import java.util.List;

public interface PNewsDetailWrapper {

	public abstract String getContent();

	public abstract Integer getPriority();

	public abstract String getNote();

	public abstract Float getRateindex();

	public abstract String getSource();

	public abstract List<String> getKeywords();

}