package models.wrappers;

public interface PStockDetailWrapper {

	public abstract String getAfterHoursChangeRealtime();

	public abstract Float getAnnualizedGain();

	public abstract Float getAsk();

	public abstract Float getAskRealtime();

	public abstract Float getAverageDailyVolume();

	public abstract Float getBid();

	public abstract Float getBidRealtime();

	public abstract Float getBookValue();

	public abstract Float getChangeFromFiftydayMovingAverage();

	public abstract Float getChangeFromTwoHundreddayMovingAverage();

	public abstract Float getChangeFromYearHigh();

	public abstract Float getChangeFromYearLow();

	public abstract Float getChangeinPercent();

	public abstract String getChangePercentRealtime();

	public abstract Float getChangeRealtime();

	public abstract Float getCommission();

	public abstract String getDaysRange();

	public abstract String getDaysRangeRealtime();

	public abstract String getDaysValueChange();

	public abstract String getDaysValueChangeRealtime();

	public abstract Float getDividendPayDate();

	public abstract Float getDividendShare();

	public abstract Float getDividendYield();

	public abstract Float getEarningsShare();

	public abstract String getEBITDA();

	public abstract Float getEPSEstimateCurrentYear();

	public abstract Float getEPSEstimateNextQuarter();

	public abstract Float getEPSEstimateNextYear();

	public abstract String getErrorIndicationreturnedforsymbolchangedinvalid();

	public abstract String getExDividendDate();

	public abstract Float getFiftydayMovingAverage();

	public abstract Float getHighLimit();

	public abstract Float getHoldingsGain();

	public abstract String getHoldingsGainPercent();

	public abstract String getHoldingsGainPercentRealtime();

	public abstract String getHoldingsGainRealtime();

	public abstract Float getHoldingsValue();

	public abstract String getHoldingsValueRealtime();

	public abstract String getLastTradeDate();

	public abstract String getLastTradePriceOnly();

	public abstract String getLastTradeRealtimeWithTime();

	public abstract String getLastTradeTime();

	public abstract String getLastTradeWithTime();

	public abstract String getLowLimit();

	public abstract String getMarketCapitalization();

	public abstract String getMarketCapRealtime();

	public abstract String getMoreInfo();

	public abstract String getStockName();

	public abstract String getNotes();

	public abstract Float getOneyrTargetPrice();

	public abstract String getOrderBookRealtime();

	public abstract String getPEGRatio();

	public abstract Float getPERatio();

	public abstract String getPERatioRealtime();

	public abstract Float getPercentChangeFromYearHigh();

	public abstract Float getPercentChange();

	public abstract Float getPercentChangeFromFiftydayMovingAverage();

	public abstract Float getPercentChangeFromTwoHundreddayMovingAverage();

	public abstract Float getPercentChangeFromYearLow();

	public abstract Float getPreviousClose();

	public abstract Float getPriceBook();

	public abstract Float getPriceEPSEstimateCurrentYear();

	public abstract Float getPriceEPSEstimateNextYear();

	public abstract Float getPricePaid();

	public abstract Float getPriceSales();

	public abstract String getSharesOwned();

	public abstract String getShortRatio();

	public abstract String getStockExchange();

	public abstract String getSymbol();

	public abstract String getTickerTrend();

	public abstract String getTradeDate();

	public abstract Float getTwoHundreddayMovingAverage();

	public abstract Float getYearHigh();

	public abstract Float getYearLow();

	public abstract String getYearRange();

}