package lhc.viewdemo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class Stock implements Data {
	public String fullCode;
	public String market;
	public String AfterHoursChangeRealtime; // N-A - N/A
	public String AnnualizedGain;
	public String Ask;
	public String AskRealtime;
	public String AverageDailyVolume;
	public String Bid;
	public String BidRealtime;
	public String BookValue;
	public String ChangeFromFiftydayMovingAverage;
	public String ChangeFromTwoHundreddayMovingAverage;
	public String ChangeFromYearHigh;
	public String ChangeFromYearLow;
	public String ChangeinPercent;
	public String ChangePercentRealtime; // N/A - -1.13%
	public String ChangeRealtime;
	public String Commission;
	public String DaysRange; // 30.45 - 30.75
	public String DaysRangeRealtime;// N/A - N/A
	public String DaysValueChange;// - - -1.13%
	public String DaysValueChangeRealtime;// N/A - N/A
	public String DividendPayDate;
	public String DividendShare;
	public String DividendYield;
	public String EarningsShare;
	public String EBITDA;// 16.912B
	public String EPSEstimateCurrentYear;
	public String EPSEstimateNextQuarter;
	public String EPSEstimateNextYear;
	public String ErrorIndicationreturnedforsymbolchangedinvalid;// Null
	public String ExDividendDate;// Aug 24
	public String FiftydayMovingAverage;
	public String HighLimit;
	public String HoldingsGain;
	public String HoldingsGainPercent; // - - -
	public String HoldingsGainPercentRealtime;
	public String HoldingsGainRealtime;
	public String HoldingsValue;
	public String HoldingsValueRealtime;
	public String LastTradeDate;
	public String LastTradePriceOnly;
	public String LastTradeRealtimeWithTime;
	public String LastTradeTime;
	public String LastTradeWithTime;
	public String LowLimit;
	public String MarketCapitalization;
	public String MarketCapRealtime;
	public String MoreInfo;
	public String stockName;
	public String Notes;
	public String OneyrTargetPrice;
	public String OrderBookRealtime;
	public String PEGRatio;
	public String PERatio;
	public String PERatioRealtime;
	public String PercentChangeFromYearHigh; // %
	public String PercentChange;
	public String PercentChangeFromFiftydayMovingAverage;
	public String PercentChangeFromTwoHundreddayMovingAverage;
	public String PercentChangeFromYearLow;
	public String PreviousClose;
	public String PriceBook;
	// public String Open; replaced by openprice
	public String PriceEPSEstimateCurrentYear;
	public String PriceEPSEstimateNextYear;
	public String PricePaid;
	public String PriceSales;
	public String SharesOwned;
	public String ShortRatio;
	public String StockExchange;
	public String symbol;
	public String TickerTrend;
	public String TradeDate;
	public String TwoHundreddayMovingAverage;
	public String YearHigh;
	public String YearLow;
	public String YearRange;
	public String changerate;
	public String changeamount;
	public String tradingvolume;
	public String daylow;
	public String dayhigh;
	public String openprice;
	public String prevclose;
	public Date time;
	public ArrayList<String> newsIDs;

	@Override
	public int getType() {
		// TODO Auto-generated method stub
		return Data.STOCK;
	}

	public Stock() {

	}

	public static Stock getFull(String source) {
		Stock stock = getBrief(source);
		return stock;
	}

	public static Stock getBrief(String source) {
		Stock stock = new Stock();
		try {
			JSONObject stockObject = new JSONObject(source);
			JSONArray idsArray = new JSONArray(stockObject.getString("newsids"));
			stock.stockName = stockObject.getString("name");
			stock.fullCode = stockObject.getString("fullcode");
			stock.market = stockObject.getString("market");
			stock.changerate = stockObject.getString("changerate");
			stock.changeamount = stockObject.getString("changeamount");
			stock.tradingvolume = stockObject.getString("tradingvolume");
			stock.dayhigh = stockObject.getString("dayhigh");
			stock.daylow = stockObject.getString("daylow");
			stock.openprice = stockObject.getString("openprice");
			stock.prevclose = stockObject.getString("prevclose");
			for (int i = 0; i < idsArray.length(); i++) {
				stock.newsIDs = new ArrayList<String>();
				stock.newsIDs.add(idsArray.get(i).toString());
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return stock;
	}
}
