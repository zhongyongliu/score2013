package lhc.viewdemo;

import android.R.integer;
import android.os.AsyncTask;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class DataService extends AsyncTask<String, integer, String> {
    private int type = -1;

    @Override
    protected String doInBackground(String... params) {
        // TODO Auto-generated method stub
        try {
            HttpClient client = new DefaultHttpClient();
            // params[0] 代表连接的url
            HttpResponse response;

            switch (type) {
                case ReturnData.REGISTER:
                    String[] ps = params[0].split("#");
                    HttpPost post = new HttpPost(ps[0]);
                    response = client.execute(post);
                    break;
                default:
                    HttpGet get = new HttpGet(params[0]);
                    response = client.execute(get);
            }
            HttpEntity entity = response.getEntity();
            InputStream is = entity.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String s = reader.readLine();
            return s;
        } catch (Exception e) {
            e.printStackTrace();
            return "{}";
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
     */
    @Override
    protected void onPostExecute(String result) {
        // TODO Auto-generated method stub
        MainActivity.showLoading();
        if (type == ReturnData.REGISTER){
            ReturnData.update(type, result);
            return;
        }
        try {
            JSONObject response = new JSONObject(result);
            if (response.getString("status").equals("Success")) {
                ReturnData.update(type, response.getString("result"));
            } else {
                ReturnData.error();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public DataService(int t) {
        type = t;
    }
}
