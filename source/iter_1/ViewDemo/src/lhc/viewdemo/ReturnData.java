package lhc.viewdemo;

import android.view.View;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ReturnData {
    public static final int GET_STOCK_BY_CODE = 0;
    public static final int GET_FAVORITE_LIST = 1;
    public static final int GET_RECOMMEND_LIST = 2;
    public static final int RELOAD_FAVORITE_LIST = 3;
    public static final int GET_STOCK_BRIEF = 4;
    public static final int GET_RECOMMEND_NEWS = 5;
    public static final int GET_NEWS_BY_ID = 6;
    public static final int REGISTER = 7;
    public static final int GET_RELATED_NEWS = 8;

    public static final String SERVER_ADDR = "http://hk.flanker017.me:9000/";

    public static void getStockByCode(String code, String option, int source) {
        execute(source, SERVER_ADDR + "api/stock/info/" + option + "/" + code);
    }

    public static void getFavoriteList() {
        execute(GET_FAVORITE_LIST,
                SERVER_ADDR
                        + "api/stock/batchinfo/brief/0001.hk,0002.HK,0003.HK,0004.HK,0005.HK,0006.HK,0007.HK,0008.HK,0008.HK,0018.HK,0028.HK");
    }

    public static void getRecommendList() {
        execute(GET_RECOMMEND_LIST,
                SERVER_ADDR
                        + "api/stock/batchinfo/brief/0011.hk,0012.HK,0045.HK,0034.HK,0056.HK,0073.HK,0055.HK,0088.HK,0038.HK,0068.HK,0118.HK,0231.HK,0039.HK");
    }

    public static void getStockBrief(String codeString) {
        getStockByCode(codeString, "brief", GET_STOCK_BRIEF);
    }

    public static void getRecommendNews() {
        execute(GET_RECOMMEND_NEWS,
                SERVER_ADDR
                        + "api/news/batchinfo/brief/372,373,374,375,376,378,379,380,381,382,383,384,385,386,387,388,389,390");
    }

    public static void getRelatedNews(String currentStockNewsSets) {
        execute(GET_RELATED_NEWS,
                SERVER_ADDR
                        + "api/news/batchinfo/brief/" + currentStockNewsSets);
    }

    public static void getNewsById(String id) {
        execute(GET_NEWS_BY_ID,
                SERVER_ADDR
                        + "api/news/info/full/" + id);
    }

    public static void reloadFavoriteList() {
        getFavoriteList();
    }

    public static void register(String username, String password) {
        execute(REGISTER,
                SERVER_ADDR
                        + "api/user/register/" + "#" + "name=" + username + "&pass=" + password);
    }

    public static void login(String username, String password) {
        execute(REGISTER,
                SERVER_ADDR
                        + "api/user/login/" + "#" + "name=" + username + "&pass=" + password);
    }

    public static void logout(String token) {
        execute(REGISTER,
                SERVER_ADDR
                        + "api/user/register/" + "#" + "token=" + token);
    }

    public static void execute(int type, String param) {
        DataService service = new DataService(type);
        service.execute(param);
    }

    public static void update(int type, String result) {
        switch (type) {
            case GET_STOCK_BRIEF:
                setStockBrief(result);
                break;
            case GET_FAVORITE_LIST:
                appendList(MainActivity.favoriteList, result, Data.STOCK);
                break;
            case GET_RECOMMEND_LIST:
                appendList(MainActivity.recoListView, result, Data.STOCK);
                break;
            case RELOAD_FAVORITE_LIST:
                setList(MainActivity.favoriteList, result, Data.STOCK);
                break;
            case GET_RECOMMEND_NEWS:
                appendList(MainActivity.newsListView, result, Data.NEWS);
                break;
            case GET_NEWS_BY_ID:
                setNewsFull(result);
                break;
            case REGISTER:
                MainActivity.successReg(result);
                break;
            case GET_RELATED_NEWS:
                MainActivity.setRelatedStockNews();
                //appendList(MainActivity.newsDetailView, result, Data.NEWS);
                MainActivity.mViewPager.setCurrentItem(1);
                break;
            default:
                break;
        }
    }

    private static void setNewsFull(String result) {
        News news;
        try {
            JSONObject newObject = new JSONObject(result);
            if (newObject != null) {
                news = News.getDetail(newObject.toString());
                MainActivity.setNewsDetailFull(news);
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            MainActivity.setNullContent();
        }
    }

    private static void setStockBrief(String result) {
        // TODO Auto-generated method stub
        Stock stock;
        try {
            JSONObject stockObject = new JSONObject(result);
            if (stockObject != null) {
                stock = Stock.getBrief(stockObject.toString());
                MainActivity.setStockDetailBrief(stock);
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            MainActivity.setNullContent();
        }
    }

    private static void setList(View tView, String result, int dataType) {
        // TODO Auto-generated method stub
        MainActivity.removeAllChildren(tView);
        appendList(tView, result, dataType);
    }

    private static void appendList(View tView, String result, int dataType) {
        // TODO Auto-generated method stub
        try {
            JSONArray dataArray = new JSONArray(result);
            if (dataArray.length() == 0) {
                MainActivity.setNullContent();
            } else {
                switch (dataType) {
                    case Data.STOCK:
                        Stock stock;
                        for (int i = 0; i < dataArray.length(); i++) {
                            stock = Stock.getBrief(dataArray.get(i).toString());
                            MainActivity.appendStock(tView, stock);
                        }
                        break;
                    case Data.NEWS:
                        News news;
                        for (int i = 0; i < dataArray.length(); i++) {
                            news = News.getBrief(dataArray.get(i).toString());
                            MainActivity.appendNews(tView, news);
                        }
                        break;
                    default:
                }
                MainActivity.hideLog();
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            MainActivity.setNullContent();
        }
    }

    public static void error() {
        // TODO Auto-generated method stub
        MainActivity.showNetworkError();
    }

}
