package lhc.viewdemo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: lhc
 * Date: 2/27/13
 * Time: 11:46 AM
 * To change this template use File | Settings | File Templates.
 */
public class News implements Data {
    public String abst;
    public String time;
    public String id;

    public String content;
    public String source;
    public ArrayList<String> keywords;

    @Override
    public int getType() {
        // TODO Auto-generated method stub
        return Data.NEWS;
    }

    public static News getBrief(String source) {
        News news = new News();
        try {
            JSONObject newsObject = new JSONObject(source);
            news.abst = newsObject.getString("abst");
            news.time = newsObject.getString("time");
            news.id = newsObject.getString("id");
            System.out.println(source);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return news;
    }

    public static News getDetail(String source) {

        News news = new News();
        try {
            JSONObject newsObject = new JSONObject(source);
            news.abst = newsObject.getString("abst");
            news.time = newsObject.getString("time");
            news.id = newsObject.getString("id");

            JSONObject detail = new JSONObject(newsObject.getString("detail"));

            news.content = detail.getString("content");
            news.source = detail.getString("source");
            JSONArray keywordsArray = new JSONArray(detail.getString("keywords"));


            for (int i = 0; i < keywordsArray.length(); i++) {
                news.keywords = new ArrayList<String>();
                news.keywords.add(keywordsArray.get(i).toString());
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return news;
    }
}
