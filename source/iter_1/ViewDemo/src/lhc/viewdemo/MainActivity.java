package lhc.viewdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.*;
import android.widget.*;

public class MainActivity extends FragmentActivity {

    public static SectionsPagerAdapter mSectionsPagerAdapter;

    public static ViewPager mViewPager;
    RelativeLayout stockDetailLayout;
    public static ScrollView favoriteList;
    public static Intent intent = new Intent();
    public static int currentNews = 0;
    public static String currentStock = "";

    public static LayoutInflater layoutInflater;

    public static View newsDetailView;
    public static View newsListView;
    public static View homeView;
    public static View recoListView;
    public static View infoView;

    public static TextView homeLogTextView;
    public static String currentStockNewsSets = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        layoutInflater = getLayoutInflater();
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the app.
        mSectionsPagerAdapter = new SectionsPagerAdapter(
                getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(2);

        // favoriteList = (ScrollView) findViewById(R.id.favorite_list);
        // intent.setClass(MainActivity.this, StockDetail.class);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        int base = Menu.FIRST;
        //getMenuInflater().inflate(R.menu.activity_main, menu);
        String[] kinds = {"All Kinds", "Financial", "Industry", "Great", "There", "Test"};
        SubMenu newsKindSubMenu = menu.addSubMenu(base, base + 1, Menu.NONE, R.string.news_kind);
        SubMenu stockKindSubMenu = menu.addSubMenu(base, base + 2, Menu.NONE, R.string.stock_kind);
        for (int i = 0; i < kinds.length; i++) {
            newsKindSubMenu.add(base + 1, base + i, i, kinds[i]);
            stockKindSubMenu.add(base + 2, base + i, i, kinds[i]);
        }
        return true;
    }


    /* (non-Javadoc)
     * @see android.support.v4.app.FragmentActivity#onMenuItemSelected(int, android.view.MenuItem)
     */
    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        CheckedTextView newsKindTextView = (CheckedTextView) newsListView.findViewById(R.id.news_list_kinds);
        CheckedTextView stockKindTextView = (CheckedTextView) recoListView.findViewById(R.id.stock_list_kind);
        switch (item.getGroupId()) {
            case 2:
                newsKindTextView.setText(item.getTitle());
                break;
            case 3:
                stockKindTextView.setText(item.getTitle());
                break;
            default:
                break;
        }
        return true;
    }

    public static void setRelatedStockNews() {
        TextView v = (TextView)(newsListView.findViewById(R.id.news_list_kinds));
        v.setText(currentStock);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a DummySectionFragment (defined as a static inner class
            // below) with the page number as its lone argument.
            Fragment fragment = new SingleSectionFragment();
            Bundle args = new Bundle();
            args.putInt(SingleSectionFragment.ARG_LAYOUT_NUMBER, position);

            fragment.setArguments(args);

            return fragment;
        }

        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.title_section0);
                case 1:
                    return getString(R.string.title_section1);
                case 2:
                    return getString(R.string.title_section2);
                case 3:
                    return getString(R.string.title_section3);
                case 4:
                    return getString(R.string.title_section4);
            }
            return null;
        }
    }

    public static class SingleSectionFragment extends Fragment {
        public static final String ARG_LAYOUT_NUMBER = "layout";

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View retView = null;
            LayoutInflater layoutInflater = getLayoutInflater(savedInstanceState);
            switch (getArguments().getInt(ARG_LAYOUT_NUMBER)) {
                case 0:
                    retView = layoutInflater.inflate(R.layout.news_detail, null);
                    newsDetailView = retView;
                    break;
                case 1:
                    retView = layoutInflater.inflate(R.layout.news_list, null);
                    setUpNews(retView);
                    newsListView = retView;
                    break;
                case 2:
                    retView = layoutInflater.inflate(R.layout.frag_home, null);
                    setUpHome(retView);
                    homeView = retView;
                    break;
                case 3:
                    retView = layoutInflater.inflate(R.layout.stock_list, null);
                    setUpReco(retView);
                    recoListView = retView;
                    break;
                case 4:
                    retView = layoutInflater.inflate(R.layout.info, null);
                    infoView = retView;
                    setUpInfo(retView);
                    break;
            }
            return retView;
        }

        private void setUpInfo(final View target) {
            Button loginBtn = (Button) target.findViewById(R.id.btn_login);
            Button registerBtn = (Button) target.findViewById(R.id.btn_register);
            final TextView userName = (TextView) target.findViewById(R.id.user_name);
            final TextView password = (TextView) target.findViewById(R.id.user_password);

            loginBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (userName.getText().length() > 0 && password.getText().length() > 0) {
                        userName.setText("Success");
                    }
                }
            });
            registerBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (userName.getText().length() > 0 && password.getText().length() > 0) {
                        //userName.setText("Get Ready!");
                        ReturnData.register(userName.getText().toString(), password.getText().toString());
                    }
                }
            });
        }

        private void setUpReco(View target) {
            ReturnData.getRecommendList();
        }

        private void setUpNews(View target) {
            ReturnData.getRecommendNews();
        }

        private void setUpHome(View target) {

            ReturnData.getFavoriteList();
            homeLogTextView = (TextView) target.findViewById(R.id.home_log);
            favoriteList = (ScrollView) target.findViewById(R.id.favorite_list);
            target.findViewById(R.id.home_log).setOnClickListener(
                    new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            ReturnData.reloadFavoriteList();
                        }
                    });
            target.findViewById(R.id.stock_detail_view).setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            favoriteList.setVisibility(View.INVISIBLE);
                        }
                    });
            target.findViewById(R.id.btn_close_stock_detail_view)
                    .setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            MainActivity.favoriteList
                                    .setVisibility(View.VISIBLE);
                        }
                    });
            target.findViewById(R.id.btn_related_news).setOnClickListener(
                    new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                           ReturnData.getRelatedNews(currentStockNewsSets);
                        }
                    });
        }
    }

    public static void appendNews(View target, News news) {

        LinearLayout layout = (LinearLayout) target.findViewById(R.id.layout);
        View newsItem = layoutInflater.inflate(R.layout.news_item, null);
        TextView abs = (TextView) (newsItem
                .findViewById(R.id.news_item_abs));
        TextView time = (TextView) (newsItem
                .findViewById(R.id.news_item_title));
        TextView id = (TextView) (newsItem.findViewById(R.id.news_item_id));
        abs.setText(news.abst);
        time.setText(news.time);
        id.setText(news.id);

        newsItem.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                currentNews = v.getId();
                TextView codeTextView = (TextView) (v
                        .findViewById(R.id.news_item_id));
                ReturnData.getNewsById(codeTextView.getText().toString());
            }
        });
        layout.addView(newsItem);
    }

    public static void appendStock(View target, Stock stock) {
        LinearLayout layout = (LinearLayout) target.findViewById(R.id.layout);
        View stockItem = layoutInflater.inflate(R.layout.stock_item, null);
        TextView name = (TextView) (stockItem
                .findViewById(R.id.list_stock_name));
        TextView changeTextView = (TextView) (stockItem
                .findViewById(R.id.list_stock_change));
        TextView code = (TextView) (stockItem
                .findViewById(R.id.list_stock_code));
        name.setText(stock.stockName);
        code.setText(stock.fullCode);
        changeTextView.setText(stock.changeamount);
        if (Double.parseDouble(stock.changeamount) > 0) {
            changeTextView
                    .setBackgroundResource(R.drawable.list_item_label_green);
        } else {
            changeTextView
                    .setBackgroundResource(R.drawable.list_item_label_red);
        }
        stockItem.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                TextView codeTextView = (TextView) (v
                        .findViewById(R.id.list_stock_code));
                ReturnData.getStockBrief(codeTextView.getText().toString());
            }
        });
        layout.addView(stockItem);
    }

    public static void removeAllChildren(View target) {
        LinearLayout layout = (LinearLayout) target.findViewById(R.id.layout);
        layout.removeAllViews();
    }

    public static void showNetworkError() {
        homeLogTextView.setText(R.string.network_error);
    }

    public static void showLoading() {
        homeLogTextView.setText("Loading...");
    }

    public static void setNullContent() {
        homeLogTextView.setText("Tap to Reload");
    }

    public static void hideLog() {
        homeLogTextView.setVisibility(View.INVISIBLE);
    }

    public static void showLog() {
        homeLogTextView.setVisibility(View.VISIBLE);
    }

    public static void successReg(String data) {
        TextView userName = (TextView) infoView.findViewById(R.id.user_name);
        userName.setText(data);
    }

    public static void setNewsDetailFull(News news) {
        TextView content = (TextView) newsDetailView.findViewById(R.id.news_detail_content);
        TextView time = (TextView) newsDetailView.findViewById(R.id.news_detail_info);
        TextView title = (TextView) newsDetailView.findViewById(R.id.news_detail_title);

        content.setText(news.content);
        time.setText(news.time);
        title.setText(news.abst.substring(0, 20) + "...");
        mViewPager.setCurrentItem(0);
    }

    public static void setStockDetailBrief(Stock stock) {
        currentStock = stock.fullCode;
        TextView stockName = (TextView) homeView.findViewById(R.id.stock_name);
        TextView stockCode = (TextView) homeView.findViewById(R.id.stock_code);
        TextView stockMarket = (TextView) homeView.findViewById(R.id.stock_market);
        TextView stockChangeAmount = (TextView) homeView.findViewById(R.id.stock_change_amount);
        TextView stockChangeRate = (TextView) homeView.findViewById(R.id.stock_change_rate);
        TextView stockTradingVolumn = (TextView) homeView.findViewById(R.id.stock_trading_volumn);
        TextView stockDayLow = (TextView) homeView.findViewById(R.id.stock_day_low);
        TextView stockDayHigh = (TextView) homeView.findViewById(R.id.stock_day_high);
        TextView stockOpenPrice = (TextView) homeView.findViewById(R.id.stock_open_price);
        TextView stockClosePrice = (TextView) homeView.findViewById(R.id.stock_close_price);

        stockName.setText(stock.stockName);
        stockCode.setText(stock.fullCode);
        stockMarket.setText(stock.market);
        stockChangeAmount.setText(stock.changeamount);
        stockChangeRate.setText(stock.changerate);
        stockTradingVolumn.setText(stock.tradingvolume);
        stockDayLow.setText(stock.daylow);
        stockDayHigh.setText(stock.dayhigh);
        stockOpenPrice.setText(stock.openprice);
        stockClosePrice.setText(stock.prevclose);
        MainActivity.favoriteList.setVisibility(View.INVISIBLE);
        currentStockNewsSets = stock.newsIDs.get(0);
        for (int i = 1; i < stock.newsIDs.size(); i++) {
            currentStockNewsSets += "," + stock.newsIDs.get(i);
        }
        mViewPager.setCurrentItem(2);
    }
}
