package lhc.viewdemo;

public interface Data {
	abstract public int getType();
	public static final int NEWS = 0;
	public static final int STOCK = 1;
	public static final int INFO = 2;
	public static final int OTHER = -1;
}
