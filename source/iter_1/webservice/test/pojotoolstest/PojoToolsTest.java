package pojotoolstest;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;

import mobilesdk.pojos.models.PStock;
import models.Stock;

import org.junit.Test;

import annotations.Detail;

import play.test.UnitTest;
import tools.ToPojoUtils;

public class PojoToolsTest extends UnitTest{

	@Test
	public void testPStockBrief()
	{
		List<Stock> stocks = Stock.findAll();
		for (Stock stock : stocks) {
			PStock pojo = ToPojoUtils.convertEntity(stock, Stock.class, PStock.class, "brief");
			assertNull(pojo.detail);
			
			Field[] fields = PStock.class.getDeclaredFields();
			for (Field field : fields) {
				int modifier = field.getModifiers();
				field.setAccessible(true);
				if (!field.isAnnotationPresent(Detail.class)) {
					try {
						assertNotNull(field.toString(), field.get(pojo));
					} catch (Exception e) {
						assertTrue("exception", false);
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	@Test
	public void testPStockDetail()
	{
		List<Stock> stocks = Stock.findAll();
		for (Stock stock : stocks) {
			PStock pojo = ToPojoUtils.convertEntity(stock, Stock.class, PStock.class,"full");
			assertNotNull(pojo.detail);
			
			Field[] fields = PStock.class.getDeclaredFields();
			for (Field field : fields) {
				field.setAccessible(true);
				if (!field.isAnnotationPresent(Detail.class)) {
					try {
						assertNotNull(field.toString(), field.get(pojo));
					} catch (Exception e) {
						assertTrue("exception", false);
						e.printStackTrace();
					}
				}
			}
		}
	}
}
