import models.Stock;

import org.junit.Test;

import play.test.FunctionalTest;

public class ApplicationTest extends FunctionalTest{
	
    @Test
    public void testStockModel()
    {
    	Stock stock = Stock.find("fullcode = ?", "3899.HK").first();
    	assertNotNull(stock);
    }
}