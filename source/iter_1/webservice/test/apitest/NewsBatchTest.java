package apitest;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

import mobilesdk.pojos.models.PNews;
import mobilesdk.pojos.models.PResult;
import mobilesdk.pojos.models.PStatus;

import org.junit.Test;

import play.mvc.Http.Response;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class NewsBatchTest extends ApiTest{

	Type type = new TypeToken<PResult<List<PNews>>>(){}.getType();
    PResult<List<PNews>> pojos;
	@Test
	public void testOrdinaryBatchInfo()
	{
		Response response = POST("/api/news/batchinfo/brief/",Collections.singletonMap("param", "1"));
        assertIsOk(response);
        pojos = gson.fromJson(response.out.toString(),type);
        assertEquals(1, pojos.getResult().size());
	}
	
	@Test
	public void testParamErrorBatchInfo()
	{
		Response response = POST("/api/news/batchinfo/brief/",Collections.singletonMap("param", "a,b"));
        assertIsOk(response);
        pojos = gson.fromJson(response.out.toString(),type);
        assertEquals(PStatus.InputParamError, pojos.getStatus());
	}
}
