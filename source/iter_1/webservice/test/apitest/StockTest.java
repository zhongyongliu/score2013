package apitest;

import java.lang.reflect.Type;

import mobilesdk.pojos.models.PResult;
import mobilesdk.pojos.models.PStatus;
import mobilesdk.pojos.models.PStock;

import org.junit.Test;

import play.mvc.Http.Response;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class StockTest extends ApiTest {

	Type type = new TypeToken<PResult<PStock>>(){}.getType();
	PResult<PStock> pojo;
	@Test
	public void testOrdinaryBriefInfo()
	{
		String[] args = {"3899.HK"};
		for (int i = 0; i < args.length; i++) {
			String code = args[i];
			Response response = GET("/api/stock/info/brief/"+code);
	        assertIsOk(response);
	        assertContentType("application/json", response);
	        
	        pojo = gson.fromJson(response.out.toString(),type );
	        assertNotNull(pojo);
	        assertEquals(pojo.getResult().fullcode, code);
		}
        
	}
	
	@Test
	public void testOrdinaryFullInfo()
	{
		String[] args = {"3899.HK"};
		for (int i = 0; i < args.length; i++) {
			String code = args[i];
			Response response = GET("/api/stock/info/full/"+code);
	        assertIsOk(response);
	        assertContentType("application/json", response);
	        pojo = gson.fromJson(response.out.toString(), type);
	        assertNotNull(pojo);
	        assertEquals(code,pojo.getResult().fullcode);
		}
	}
	
	
}
