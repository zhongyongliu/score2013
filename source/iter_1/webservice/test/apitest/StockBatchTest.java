package apitest;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

import mobilesdk.pojos.models.PResult;
import mobilesdk.pojos.models.PStatus;
import mobilesdk.pojos.models.PStock;

import org.junit.Test;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import play.mvc.Http.Response;
import play.test.FunctionalTest;

public class StockBatchTest extends ApiTest {

	Type type = new TypeToken<PResult<List<PStock>>>(){}.getType();
	
	@Test
	public void testOrdinaryStockBatch() {
		Response response;
		
		//2 
		response = POST("/api/stock/batchinfo/brief/",Collections.singletonMap("param", "3899.HK,3190.HK"));
        assertIsOk(response);
        PResult<List<PStock>> pojos = gson.fromJson(response.out.toString(),type);
        assertEquals(2, pojos.getResult().size());
        assertEquals("3899.HK", pojos.getResult().get(0).fullcode);
        assertEquals("3190.HK", pojos.getResult().get(1).fullcode);

        //0
        response = POST("/api/stock/batchinfo/brief/",Collections.singletonMap("param", "39.HK,3.HK"));
        assertIsOk(response);
        pojos = gson.fromJson(response.out.toString(),type);
        assertEquals(0, pojos.getResult().size());
        assertEquals(PStatus.NotFound, pojos.getStatus());
        
        //1
        response = POST("/api/stock/batchinfo/brief/",Collections.singletonMap("param", "3190.HK,3.HK"));
        assertIsOk(response);
        pojos = gson.fromJson(response.out.toString(),type);
        assertEquals(1, pojos.getResult().size());
	}
	
	@Test
	public void testRandomStockBatch()
	{
		while(fre-- != 0)
        {
        	POST("/api/stock/batchinfo/brief/",Collections.singletonMap("param", randomGen(fre)));
        	POST("/api/stock/batchinfo/full/",Collections.singletonMap("param", randomGen(fre)));
        }
	}
}
