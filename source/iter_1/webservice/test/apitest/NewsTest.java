package apitest;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import mobilesdk.pojos.models.PNews;
import mobilesdk.pojos.models.PResult;

import org.junit.Test;



import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import play.mvc.Http.Response;
import play.test.FunctionalTest;

public class NewsTest extends ApiTest {

	@Test
	public void testOrdinaryBriefInfo()
	{
			Response response = GET("/api/news/info/brief/1");
			assertIsOk(response);
			assertContentType("application/json", response);
			PResult<PNews> pojo = gson.fromJson(response.out.toString(), new TypeToken<PResult<PNews>>(){}.getType());
			assertEquals("google and baidu", pojo.getResult().abst);
			assertNull(pojo.getResult().detail);
			
	}
	@Test
	public void testRandomBriefInfo()
	{
		for(int i=0;i<fre;i++)
			GET("/api/stock/info/brief/"+randomGen(i));
	}
	
	@Test
	public void testOrdinaryFullInfo()
	{
		Response response = GET("/api/news/info/full/1");
		assertIsOk(response);
		assertContentType("application/json", response);
		PResult<PNews> pojo = gson.fromJson(response.out.toString(), new TypeToken<PResult<PNews>>(){}.getType());
		assertNotNull(pojo.getResult().detail);
		List<String> keywords = Arrays.asList("google","baidu");
		assertEquals(keywords, pojo.getResult().detail.keywords);
		
	}
	
	@Test
	public void testRandomFullInfo()
	{
		for(int i=0;i<fre;i++)
			GET("/api/stock/info/full/"+randomGen(i));
	}
	
	
	
}
