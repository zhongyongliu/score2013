package apitest.utils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import play.mvc.Http.Header;
import play.mvc.Http.Request;
import play.mvc.Http.Response;
import play.test.FunctionalTest;

public class TestUtils {

	public static Response makePostWithHeader(String url,String key,String value, Map<String, String> params)
	{
		Request request = FunctionalTest.newRequest();
		request.headers.put(key, new Header(key, value));
		return FunctionalTest.POST(request,url,params,new HashMap<String, File>());
	}
	
	public static Response makeGetWithHeader(String url,String key,String value)
	{
		Request request = FunctionalTest.newRequest();
		request.headers.put(key, new Header(key, value));
		return FunctionalTest.GET(request,url);
	}
	
	public static Response makeDeleteWithHeader(String url, String key, String value)
	{
		Request request = FunctionalTest.newRequest();
		request.headers.put(key, new Header(key, value));
		return FunctionalTest.DELETE(request, url);
	}
	
	public static Response makePutWithHeader(String url, String key, String value)
	{
		Request request = FunctionalTest.newRequest();
		request.headers.put(key, new Header(key, value));
		return FunctionalTest.PUT(request, url, FunctionalTest.APPLICATION_X_WWW_FORM_URLENCODED, "");
	}
}
