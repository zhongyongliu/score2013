package apitest;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

import mobilesdk.pojos.models.PResult;
import mobilesdk.pojos.models.PStatus;
import mobilesdk.pojos.models.PStockSearchResult;

import org.junit.Test;

import play.mvc.Http.Response;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class StockSearchTest extends ApiTest {


	Type type = new TypeToken<PResult<List<PStockSearchResult>>>(){}.getType();
	PResult<List<PStockSearchResult>> array;
	@Test
	public void testNotFoundPreciseSearch()
	{
		Response response = POST("/api/stock/search/precise/",Collections.singletonMap("content", "@@@"));
		assertIsOk(response);
		assertContentType("application/json", response);
		array = gson.fromJson(response.out.toString(), type);
		assertEquals(0, array.getResult().size());
		assertEquals(PStatus.NotFound, array.getStatus());
	}
	
	@Test
	public void testOrdinaryPreciseSearch()
	{
		//2
		Response response = POST("/api/stock/search/precise/",Collections.singletonMap("content", "HK"));
		assertIsOk(response);
		assertContentType("application/json", response);
		array = gson.fromJson(response.out.toString(), type);
		assertEquals(2, array.getResult().size());
		PStockSearchResult result1 = array.getResult().get(0);
		PStockSearchResult result2 = array.getResult().get(1);
		assertNotNull(result1);
		assertNotNull(result2);
		assertTrue(result1.stockcode.contains("HK"));
		assertTrue(result2.stockcode.contains("HK"));
		
		//1
		response = POST("/api/stock/search/precise/",Collections.singletonMap("content", "MT"));
		assertIsOk(response);
		assertContentType("application/json", response);
		array = gson.fromJson(response.out.toString(), type);
		assertEquals(1, array.getResult().size());
		result1 = array.getResult().get(0);
		assertNotNull(result1);
		assertTrue(result1.stockname.contains("MT"));
	}
	
	@Test
	public void testRandomPreciseSearch()
	{
		for (int i = 0; i < fre; i++) {
			POST("/api/stock/search/precise/",Collections.singletonMap("content", randomGen(i)));
		}
	}
	
	@Test
	public void testRandomLabelSearch()
	{
		for (int i = 0; i < fre; i++) {
			POST("/api/stock/search/label/",Collections.singletonMap("content", randomGen(i)));
		}
	}
	@Test
	public void testOrdinaryLabelSearch()
	{
		Response response = POST("/api/stock/search/label/",Collections.singletonMap("content", "transport"));
		assertIsOk(response);
		assertContentType("application/json", response);
		array = gson.fromJson(response.out.toString(), type);
		assertEquals(1, array.getResult().size());
		PStockSearchResult result1 = array.getResult().get(0);
		assertNotNull(result1);
	}
	@Test
	public void testNotFoundLabelSearch()
	{
		Response response = POST("/api/stock/search/label/",Collections.singletonMap("content", "zzz"));
		assertIsOk(response);
		assertContentType("application/json", response);
		array = gson.fromJson(response.out.toString(), type);
		assertEquals(0, array.getResult().size());
	}
}
