package apitest;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import mobilesdk.pojos.models.PResult;
import mobilesdk.pojos.models.PStatus;

import org.junit.Before;
import org.junit.Test;

import play.mvc.Http.Response;
import play.test.FunctionalTest;

import apitest.utils.TestUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import controllers.api.PreController;

public class UserTest extends FunctionalTest{
	
	@Test
	public void testLogin()
	{
		PResult<String> result = null;
		Map<String, String> params = new HashMap<String, String>();
		Type type = new TypeToken<PResult<String>>(){}.getType();
		Gson gson = new Gson();
		
		params.put("name", "testuser");
		params.put("pass", "testuser");
		Response response = POST("/api/user/login/",params);
		assertIsOk(response);
		result = gson.fromJson(response.out.toString(), type);
		assertNotNull(result);
		assertEquals(PStatus.Success, result.getStatus());
		assertNotNull(result.getResult());
		
		params.clear();
		
	}
	
	@Test
	public void testLogout()
	{
		PResult<String> result = null;
		Map<String, String> params = new HashMap<String, String>();
		Type type = new TypeToken<PResult<String>>(){}.getType();
		Gson gson = new Gson();
		
		params.put("name", "testuser");
		params.put("pass", "testuser");
		Response response = POST("/api/user/login/",params);
		assertIsOk(response);
		result = gson.fromJson(response.out.toString(), type);
		assertNotNull(result);
		assertEquals(PStatus.Success, result.getStatus());
		assertNotNull(result.getResult());
		params.clear();
		
		String token = result.getResult();
		response = TestUtils.makePostWithHeader("/api/user/logout/", PreController.AUTHTOKEN, token, params);
		result = gson.fromJson(response.out.toString(), type);
		assertNotNull(result);
		assertEquals(PStatus.Success, result.getStatus());
		
		params.put("name", "testuser");
		params.put("pass", "testuser");
		response = POST("/api/user/login/",params);
		assertIsOk(response);
		result = gson.fromJson(response.out.toString(), type);
		assertNotNull(result);
		assertEquals(PStatus.Success, result.getStatus());
		assertNotNull(result.getResult());
		assertFalse(token.equals(result.getStatus()));
		
		String newtoken = result.getResult();
		response = POST("/api/user/logout/",Collections.singletonMap("token", token));
		result = gson.fromJson(response.out.toString(), type);
		assertNotNull(result);
		assertEquals(PStatus.AuthError, result.getStatus());
	}

	@Test
	public void testFav()
	{
		PResult<String> result = null;
		Map<String, String> params = new HashMap<String, String>();
		Type type = new TypeToken<PResult<String>>(){}.getType();
		Gson gson = new Gson();
		
		params.put("name", "testuser");
		params.put("pass", "testuser");
		Response response = POST("/api/user/login/",params);
		assertIsOk(response);
		result = gson.fromJson(response.out.toString(), type);
		assertNotNull(result);
		assertEquals(PStatus.Success, result.getStatus());
		String token = result.getResult();
		assertNotNull(result.getResult());
		
		params.clear();
		
		String code = "3190.HK";
		response = TestUtils.makePutWithHeader("/api/testuser/"+code+"/favorite",PreController.AUTHTOKEN,token); //= POST(request,"/api/label/"+code+"/",params);
		assertIsOk(response);
		System.out.println(response.out.toString());
		
		//TestUtils.makePutWithHeader("http://www.baidu.com", "fuck", "fuck");
	}
}
