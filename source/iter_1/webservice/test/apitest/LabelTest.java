package apitest;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mobilesdk.pojos.models.PLabel;
import mobilesdk.pojos.models.PResult;
import mobilesdk.pojos.models.PStatus;
import models.Label;
import models.Stock;
import models.User;

import org.junit.Before;
import org.junit.Test;

import play.mvc.Http.Header;
import play.mvc.Http.Request;
import play.mvc.Http.Response;
import play.test.FunctionalTest;

import apitest.utils.TestUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import controllers.api.PreController;

public class LabelTest extends ApiTest {
	String token;
	
	@Before
	public void setUp() {
		PResult<String> result = null;
		Map<String, String> params = new HashMap<String, String>();
		Type type = new TypeToken<PResult<String>>(){}.getType();
		
		params.put("name", "testuser");
		params.put("pass", "testuser");
		Response response = POST("/api/user/login/",params);
		assertIsOk(response);
		result = gson.fromJson(response.out.toString(), type);
		assertNotNull(result);
		assertEquals(PStatus.Success, result.getStatus());
		assertNotNull(result.getResult());
		
		params.clear();
		token = result.getResult();
	}
	@Test
	public void testAdd()
	{
		PResult<String> result = null;
		Map<String, String> params = new HashMap<String, String>();
		Type type = new TypeToken<PResult<String>>(){}.getType();
		String code = "3899.HK";
		params.put("content", "nice stock");
		Response response = TestUtils.makePostWithHeader("/api/label/"+code+"/",PreController.AUTHTOKEN,token, params); //= POST(request,"/api/label/"+code+"/",params);
		
		assertIsOk(response);
		
		result = gson.fromJson(response.out.toString(), type);
		assertNotNull(result);
		assertEquals(result.getResult(), PStatus.Success, result.getStatus());
		Long id = Long.valueOf(result.getResult());
		
		Stock stock = Stock.find("fullcode =?", code).first();
		User user = User.find("token = ?", token).first();
		
		Label label = Label.find("id = ?", id).first();
		assertNotNull(label);
		assertNotNull(label.user);
		assertNotNull(label.stock);
		assertEquals(code, label.stock.fullcode);
		assertEquals(token, label.user.token);
		
		//add again, should throw an error
		response = TestUtils.makePostWithHeader("/api/label/"+code+"/",PreController.AUTHTOKEN,token, params); //= POST(request,"/api/label/"+code+"/",params);
		assertIsOk(response);
		result = gson.fromJson(response.out.toString(), type);
		assertNotNull(result);
		assertEquals(result.getResult(), PStatus.AlreadyExist, result.getStatus());
		
	}
	
	@Test
	public void testGetAndDelete()
	{
		PResult<List<PLabel>> result = null;
		Type type = new TypeToken<PResult<List<PLabel>>>(){}.getType();
		String code = "3899.HK";
		 
		//get labels by stock
		Response response = TestUtils.makeGetWithHeader("/api/label/"+code+"/",PreController.AUTHTOKEN,token);
		assertIsOk(response);
		
		result = gson.fromJson(response.out.toString(), type);
		assertNotNull(result);
		assertEquals(PStatus.Success,result.getStatus());
		assertTrue(result.getResult().size() != 0);
		//delete labels
		type = new TypeToken<PResult<String>>(){}.getType();
		for (PLabel label : result.getResult()) {
			System.out.println(label.id);
			response = TestUtils.makeDeleteWithHeader("/api/label/"+code+"/"+label.id, PreController.AUTHTOKEN, token);
			assertIsOk(response);
			result = gson.fromJson(response.out.toString(), type);
			System.out.println(response.out.toString());
			assertEquals(result.getReason(),PStatus.Success, result.getStatus());
		}
		
		type = new TypeToken<PResult<List<PLabel>>>(){}.getType();
		response = TestUtils.makeGetWithHeader("/api/label/"+code+"/",PreController.AUTHTOKEN,token);
		assertIsOk(response);
		result = gson.fromJson(response.out.toString(), type);
		assertNotNull(result);
		assertEquals(PStatus.NotFound,result.getStatus());
		assertTrue(result.getResult().size() == 0);
	}
}
