package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import controllers.api.TokenNeeded;
import controllers.api.PreController;

import models.*;
@With(PreController.class)
public class Application extends Controller {

	@TokenNeeded
    public static void index() {
        renderText("For APIs, please refer to https://trello.com/board/score-api-document/50c3f72561e550965c0000ad");
    }

}