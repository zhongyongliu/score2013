package controllers.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mobilesdk.pojos.models.PResult;
import mobilesdk.pojos.models.PStatus;
import mobilesdk.pojos.models.PStock;
import models.Stock;
import models.User;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.RandomStringUtils;

import play.data.validation.Email;
import play.data.validation.Match;
import play.data.validation.Required;
import play.libs.Codec;
import play.mvc.Controller;
import play.mvc.With;
import play.data.validation.Error;
import tools.ResultBuilder;
import tools.ToPojoUtils;

@With(PreController.class)
public class user extends Controller {
	
	@TokenNeeded
	public static void info()
	{
		renderText(PreController.getCurrentUser().name);
	}
	public static void login(@Required @Match("[A-Za-z0-9_]+") String name, @Required String pass) {
		PResult<String> ret = new PResult<String>();
		if (validation.hasErrors()) {
			ret.setStatus(PStatus.InputParamError);
			StringBuilder builder = new StringBuilder();
			for (Error error : validation.errors()) {
				builder.append(error.message());
				builder.append(',');
			}
			ret.setReason(builder.toString());
			renderJSON(ret);
		}
		String hash = Codec.hexMD5(DigestUtils.md5Hex(name)
				+ Codec.hexMD5(pass));
		User user = User.find("name = ? and passhash = ?", name, hash).first();
		if (user == null) {
			ret.setStatus(PStatus.AuthError);
		} else {
			ret.setStatus(PStatus.Success);
			ret.setResult(user.token);
		}
		renderJSON(ret);
	}

	@TokenNeeded
	public static void logout() {
		PResult<String> ret = new PResult<String>();
		User user = PreController.getCurrentUser();
		String prevToken = user.token;
		String nowToken = genToken(user);
		while (prevToken.equals(nowToken)) {
			nowToken = genToken(user);
		}
		user.token = nowToken;
		user.save();
		ret.setStatus(PStatus.Success);
		renderJSON(ret);
	}

	private static String genToken(User user)
	{
		return Codec.hexMD5(Codec.hexMD5(user.name) + RandomStringUtils.randomAlphanumeric(5)
				+ Codec.hexMD5(user.passhash));
	}
	
	private static String genHash(String name,String pass)
	{
		return Codec.hexMD5(Codec.hexMD5(name)
				//+ RandomStringUtils.randomAlphanumeric(5)
				+ Codec.hexMD5(pass));
	}
	
	final static String DUPLICATE_USER_NAME = "duplicate user name";
	public static void register(@Required @Match("[A-Za-z0-9_]+") String name, @Required String pass, @Email String email) {
		PResult<String> ret = new PResult<String>();
		if (validation.hasErrors() || name.equals(User.getSystemUserName())) {
			ret.setStatus(PStatus.InputParamError);
			StringBuilder builder = new StringBuilder();
			for (Error error : validation.errors()) {
				builder.append(error.message());
				builder.append(',');
			}
			ret.setReason(builder.toString());
			renderJSON(ret);
		}
		long cnt = User.count("name = ?", name);
		if (cnt != 0) {
			ret.setStatus(PStatus.AlreadyExist);
			ret.setReason(DUPLICATE_USER_NAME);
		} else {
			String hash = genHash(name, pass);
			User user = new User();
			user.name = name;
			user.passhash = hash;
			user.token = genToken(user);
			user.email = email;
			user.regtime = new Date();
			user.save();
			ret.setStatus(PStatus.Success);
			ret.setResult(user.token);
		}
		renderJSON(ret);
	}
	
	@TokenNeeded
	@InitStock
	public static void likeStock()
	{
		User user = PreController.getCurrentUser();
		Stock stock = PreController.getCurrentStock();
		
		user.favstocks.add(stock);
		user.save();
		
		renderJSON(ResultBuilder.buildResult(""));
	}
	
	@TokenNeeded
	@InitStock
	public static void dislikeStock()
	{
		User user = PreController.getCurrentUser();
		Stock stock = PreController.getCurrentStock();
		
		user.favstocks.remove(stock);
		user.save();
		
		renderJSON(ResultBuilder.buildResult(""));
	}
	
	@TokenNeeded
	public static void favStockList()
	{
		User user = PreController.getCurrentUser();
		List<PStock> stocks = new ArrayList<PStock>();
		for (Stock stock : user.favstocks) {
			stocks.add(ToPojoUtils.convertEntity(stock, Stock.class, PStock.class,
					"brief"));
		}
		
		renderJSON(ResultBuilder.buildResult(stocks));
	}
	
	@TokenNeeded
	public static void recomStockList()
	{
		User user = PreController.getCurrentUser();
		List<PStock> stocks = new ArrayList<PStock>();
		
		//stub code begins
		if (user.recomStocks == null) {
			user.recomStocks = new ArrayList<Stock>();
		}
		if (user.recomStocks.size() == 0) {
			user.recomStocks.add(PreController.stubStock);
		}
		//stub code ends
		for (Stock stock : user.recomStocks) {
			stocks.add(ToPojoUtils.convertEntity(stock, Stock.class, PStock.class,
					"brief"));
		}
		
		renderJSON(ResultBuilder.buildResult(stocks));
	}

}
