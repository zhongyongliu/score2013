package controllers.api;

import java.util.ArrayList;
import java.util.List;

import mobilesdk.pojos.models.PNews;
import mobilesdk.pojos.models.PResult;
import mobilesdk.pojos.models.PStatus;
import models.Stock;
import models.StockNews;
import play.data.binding.As;
import play.mvc.Controller;
import play.mvc.With;
import tools.ResultBuilder;
import tools.ToPojoUtils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.sun.tools.internal.xjc.reader.xmlschema.bindinfo.BIConversion.Static;

/**
 * @author hqdvista
 * Controller for news api
 */
public class news extends Controller {

	private static final String NO_SUCH_STOCK = "no such stock";
	/**
	 * @param param news id, LONG
	 * @param type "brief" or "full"
	 */
	public static void info(Long param, String type) {
		StockNews news = StockNews.find("id = ?", param).first();
		
		renderJSON(ResultBuilder.buildResult(ToPojoUtils.convertEntity(news, StockNews.class,
				PNews.class, type)));
	}

	/**
	 * @param param list of news ids, LONGs
	 * @param type "brief" or "full"
	 */
	final static String PARAM_IDS_PARSE_ERROR = "cannot parse param list, should be a list of long";
	public static void batchinfo(@As(",") List<Long> param, String type) {
		
		List<PNews> pojos = new ArrayList<PNews>();
		List<Long> ids = param;
		if (ids != null && ids.size() != 0) {
			if (!(ids.size() == 1 && ids.get(0) == null)) {
				//test case: input => param=
				// the ids will be [null]
				List<StockNews> newses = StockNews.find("id in (:ids)")
						.bind("ids", ids).fetch();
				for (StockNews news : newses) {
					pojos.add(ToPojoUtils.convertEntity(news, StockNews.class,
							PNews.class, type));
				}
			}
			renderJSON(ResultBuilder.buildResult(pojos));
		}
		else {
			renderJSON(ResultBuilder.errorParam(PARAM_IDS_PARSE_ERROR));
		}
	}
	
	public static void getNewsByCode(String code)
	{
		Stock stock = Stock.find("fullcode = ?", code).first();
		if (stock == null) {
			renderJSON(ResultBuilder.NoSuchParam(NO_SUCH_STOCK));
		}
		
		List<StockNews> newses = stock.stocknews;
		List<Long> ret = new ArrayList<Long>();
		for (StockNews stockNews : newses) {
			ret.add(stockNews.id);
		}
		
		renderJSON(ResultBuilder.buildResult(ret));
	}
}
