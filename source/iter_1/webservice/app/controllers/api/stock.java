package controllers.api;

import java.util.ArrayList;
import java.util.List;

import mobilesdk.pojos.models.PResult;
import mobilesdk.pojos.models.PStatus;
import mobilesdk.pojos.models.PStock;
import mobilesdk.pojos.models.PStockSearchResult;
import models.Label;
import models.Stock;
import models.User;
import play.data.binding.As;
import play.mvc.Controller;
import play.mvc.With;
import tools.ResultBuilder;
import tools.ToPojoUtils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
/**
 * @author hqdvista
 * Controller for stock related API
 *
 */
@With(PreController.class)
public class stock extends Controller {
	

	/**
	 * @param param fullcode of requested stock
	 * @param type "brief" or "full"
	 */
	public static void info(String param, String type) {
		Stock stock = Stock.find("fullcode = ?", param).first();
		renderJSON(ResultBuilder.buildResult(ToPojoUtils.convertEntity(stock, Stock.class, PStock.class,
				type)));
	}

	
	/**
	 * @param param list of fullcodes of requested stocks
	 * @param type "brief" or "full"
	 */
	public static void batchinfo(@As(",") List<String> param, String type) {
		List<String> codes = param;
		List<PStock> pojos = new ArrayList<PStock>();
		if (codes != null) {
			List<Stock> stocks = Stock.find("fullcode in (:codes)")
					.bind("codes", codes).fetch();
			for (Stock stock : stocks) {
				pojos.add(ToPojoUtils.convertEntity(stock, Stock.class,
						PStock.class, type));
			}
			renderJSON(ResultBuilder.buildResult(pojos));
		}
		else {
			renderJSON(ResultBuilder.errorParam("cannot parse param list, should be a list of strings"));
		}
	}

	// TODO: use lucene search module in Play! Framework
	/**
	 * @param content content to search
	 * @param type "label" or "precise", "precise" will conduct search in stock fullcode or name,
	 * 				"label" will conduct search in stock labels
	 */
	public static void search(String content, String type) {
		List<PStockSearchResult> results = new ArrayList<PStockSearchResult>();
		if (content == null || content.isEmpty()) {// check if search content is
													// null
			renderJSON(ResultBuilder.errorParam("content cannot be empty"));
		}
		List<Stock> temp = Stock.find("fullcode like ?1 or name like ?2",
				'%' + content + '%', '%' + content + '%').fetch();
		if (type.equals("precise")) {
			for (Stock stock : temp) {
				results.add(ToPojoUtils.convertEntity(stock, content, null));
			}
			renderJSON(ResultBuilder.buildResult(results));
		} else if (type.equals("label")) {// may search by system and user label
			User system = User.getSystemUser();

			List<Stock> stocks = Stock
					.find("select distinct s from Stock s inner join s.labels as labels, Label l where l in labels and l.content like ? and ( l.user = ?2)",
							'%' + content + '%', system).fetch();
			for (Stock stock : stocks) {
				List<Label> labels = Label.find("stock = ? and content like ?",
						stock, '%' + content + '%').fetch();
				results.add(ToPojoUtils.convertEntity(stock, content, labels));
			}
			renderJSON(ResultBuilder.buildResult(results));
		}
	}
}
