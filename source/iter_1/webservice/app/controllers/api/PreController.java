package controllers.api;

import mobilesdk.pojos.models.PResult;
import mobilesdk.pojos.models.PStatus;
import models.Stock;
import models.User;
import play.cache.Cache;
import play.mvc.*;
import play.mvc.Http.Header;
import tools.ResultBuilder;

public class PreController extends Controller {

    private static User currentUser;
    private static Stock currentStock;
    public static final String AUTHTOKEN = "authtoken";
    public static Stock stubStock = Stock.findByCode("0066.HK");
    
	@Before
    public static void preprocess() {
		
		TokenNeeded secure = getActionAnnotation(TokenNeeded.class);
		if (secure != null) {
			Header tokenhead = request.headers.get(AUTHTOKEN);
			String token = null;
			if (tokenhead != null) {
				token = tokenhead.values.get(0);
			}
			else if (!(params.get("token") == null)) {
				token = params.get("token");
			}
			System.out.println(token);
		    if (token == null || (currentUser = User.getUserByToken(token)) == null) {
		        	renderJSON(ResultBuilder.AuthError("invalid token"));
				}
			}
		InitStock anno = getActionAnnotation(InitStock.class);
		if (anno != null) {
			String code = params.get("code");
			if (code == null) {
				renderJSON(ResultBuilder.NoSuchParam("code not specified"));
			}
			currentStock = Stock.find("fullcode = ?",code).first();
	    	if (currentStock == null) {
	    		renderJSON(ResultBuilder.NoSuchParam("code not specified"));
			}
		}
    }
	public static User getCurrentUser() {
		return currentUser;
	}
	public static Stock getCurrentStock()
	{
		return currentStock;
	}
}
