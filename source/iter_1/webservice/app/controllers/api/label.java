package controllers.api;

import java.util.ArrayList;
import java.util.List;

import mobilesdk.pojos.models.PLabel;
import models.Label;
import models.Stock;
import play.mvc.*;
import tools.ResultBuilder;
import tools.ToPojoUtils;
/**
 * @author hqdvista
 * controller class for label API
 * PreController will intercept token and target stock
 */
@With(PreController.class)
public class label extends Controller {

	/**
	 * @param content 
	 * add label
	 */
	final static String DUPLICATE_LABEL = "lable duplicate";
	@TokenNeeded
	@InitStock
    public static void add(String content)
    {
    	Label label = Label.findByUserAndStockAndContent(PreController.getCurrentUser(), PreController.getCurrentStock(), content);
    	if (label != null) {
			//already exists
    		renderJSON(ResultBuilder.duplicateParam(DUPLICATE_LABEL));
		}

    	//reverse direction is wrong!
    	label = new Label(content,PreController.getCurrentUser(),PreController.getCurrentStock());
    	label.save();
    	
    	renderJSON(ResultBuilder.buildResult(label.id));
    }

	/**
	 * @param id 
	 * label id to delete
	 * 
	 */
	@TokenNeeded
	@InitStock
	public static void delete(Long id)
	{
    	Label label = Label.find("stock = ? and id = ? and user = ?", PreController.getCurrentStock(),id,PreController.getCurrentUser()).first();
    	if (label == null) {
			renderJSON(ResultBuilder.errorParam("no such label"));
		}
    	else {
			label.delete();
			renderJSON(ResultBuilder.buildResult(""));//don't use null, or ResultBuilder will render NotFound
		}
	}
	
	/**
	 * get labels by stock and user
	 */
	@TokenNeeded
	@InitStock
	public static void get()
	{
    	List<Label> labels = Label.find("stock = ? and user = ?", PreController.getCurrentStock(),PreController.getCurrentUser()).fetch();
    	List<PLabel> pojos = new ArrayList<PLabel>();
    	for (Label label : labels) {
			pojos.add(ToPojoUtils.convertEntity(label));
		}
    	renderJSON(ResultBuilder.buildResult(pojos));
	}
}
