package tools;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;



import mobilesdk.pojos.models.PLabel;
import mobilesdk.pojos.models.PNews;
import mobilesdk.pojos.models.PNewsDetail;
import mobilesdk.pojos.models.PStock;
import mobilesdk.pojos.models.PStockSearchResult;
import models.Label;
import models.News;
import models.Stock;
import models.StockNews;

public class ToPojoUtils {

	private final static String BRIEF = "brief";
	private final static String FULL  = "FULL";
	private static String generateGetter(String name)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("get");
		if(name.length() != 0)
		{
			sb.append(Character.toUpperCase(name.charAt(0)));
			sb.append(name.substring(1));
		}
		return sb.toString();
	}
	public static <T, N> T convertEntity(N input,Class<N> iclass,Class<T> oclass,String type)
	{
		boolean isBrief = type.equals(ToPojoUtils.BRIEF);
		if (input == null) {
			return null;
		}
		try {
			T t = oclass.newInstance();
			Field[] fields = oclass.getDeclaredFields();
			for (Field field : fields) {
				int modifier = field.getModifiers();
				if (field.isAnnotationPresent(annotations.Detail.class)) {
					if(!isBrief){
						field.set(t, convertEntity(input, iclass, field.getType(),ToPojoUtils.FULL));
					}
				}
				else if(!(Modifier.isFinal(modifier) || Modifier.isStatic(modifier))){
					Method method = iclass.getDeclaredMethod(generateGetter(field.getName()), null);
					field.set(t, method.invoke(input));
				}
			}
			return t;
		} catch (Exception e) {
			System.out.println(oclass);
			e.printStackTrace();
			return null;
		}
	}
	
	public static PStockSearchResult convertEntity(Stock stock, String content, List<Label> matchedLabels)
	{
		PStockSearchResult result = new PStockSearchResult(stock.getFullcode(),stock.getName());
		if(matchedLabels != null)
		{
			result.labels = new ArrayList<String>();
			for (Label label : matchedLabels) {
				result.labels.add(label.getContent());
			}
		}
		return result;
	}
	
	public static PLabel convertEntity(Label label)
	{
		PLabel pl = new PLabel();
		pl.content = label.content;
		pl.id = label.id;
		return pl;
	}
}
