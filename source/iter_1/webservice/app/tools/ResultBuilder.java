package tools;

import java.util.Collection;

import mobilesdk.pojos.models.PResult;
import mobilesdk.pojos.models.PStatus;

/**
 * @author hqdvista
 * HelperClass for build JSON wrapper Results
 */
public class ResultBuilder {

	public static  <T> PResult buildResult(T income)
	{
		if (income == null) {
			return new PResult(PStatus.NotFound,income);
		}
		else {
			return new PResult(PStatus.Success,income);
		}
	}
	
	public static <T extends Collection> PResult buildResult(T income)
	{
		if (income == null || income.size() == 0) {
			return new PResult(PStatus.NotFound,income);
		}
		else {
			return new PResult(PStatus.Success,income);
		}
	}
	
	public static PResult errorParam(String reason)
	{
		return new PResult(PStatus.InputParamError,null,reason);
	}
	
	public static PResult duplicateParam(String reason)
	{
		return new PResult(PStatus.AlreadyExist,null,reason);
	}
	
	public static PResult NoSuchParam(String reason)
	{
		return new PResult(PStatus.NotFound,null,reason);
	}
	
	public static  PResult AuthError(String reason)
	{
		return new PResult(PStatus.AuthError,null,reason);
	}
}
