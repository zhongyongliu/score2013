package jobs;

import models.TimeStockData;
import play.Logger;
import play.jobs.Job;
import play.jobs.On;
import play.test.Fixtures;
/** Fire at 09:58am every day to clear last day's stock data**/ 
@On("0 55 9 * * ?")
public class ClearStockDataJob extends Job {

	public void doJob()
	{
		Logger.info("clearing time data");
		Fixtures.delete(TimeStockData.class);
		Logger.info("job done");
	}
}
