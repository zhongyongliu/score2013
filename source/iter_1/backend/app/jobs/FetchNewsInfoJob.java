package jobs;

import java.util.Iterator;
import java.util.List;

import play.Logger;
import play.db.jpa.JPA;
import play.jobs.Job;
import backend.fetch.HKStockFetcherImpl;
import backend.fetch.StockFetcher;
import backend.pojos.GNews;

public class FetchNewsInfoJob extends Job {
	
	@Override
	public void doJob()
	{
		Logger.info("fetch stock info job begin");
		
		StockFetcher fetcher = new HKStockFetcherImpl();
		List<GNews> news = fetcher.getStockNews(fetcher.getStockCodes());
		
		int count = 0;
		for(int i=0;i<news.size();i++)
		{
			news.get(i).store(false, "");
			count++;
			if (count == 100) {
				JPA.em().flush();
				JPA.em().clear();
				count = 0;
			}
			//if store to file, set here
		}
	}

}
