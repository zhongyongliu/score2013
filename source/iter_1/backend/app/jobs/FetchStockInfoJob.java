package jobs;

import java.util.ArrayList;
import java.util.List;

import play.Logger;
import play.db.jpa.JPA;
import play.jobs.Every;
import play.jobs.Job;
import play.jobs.On;
import play.test.Fixtures;
import backend.fetch.HKStockFetcherImpl;
import backend.fetch.StockFetcher;
import backend.pojos.JGeneral;
import backend.pojos.JStock;
//fire at every two minute from 10:00am to 4:00 pm
@On("0 */2 10-16 * * ?")
public class FetchStockInfoJob extends Job {
	
	public void doJob()
	{
		//may have to deal with null here
		Logger.info("fetch stock info job begin");
		StockFetcher fetcher = new HKStockFetcherImpl();
		List<String> stockcodes = fetcher.getStockCodes();
		List<JStock> stocks= fetcher.getStockData(stockcodes);
		int count = 0;
		for (int i = 0; i < stocks.size(); i++) {
			count++;
			if (count == 100) {
				JPA.em().flush();
				JPA.em().clear();
				count = 0;
			}
			stocks.get(i).store();
		}
		Logger.info("job done");
	}

}
