package backend.loader;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import play.Play;
import play.classloading.ApplicationClasses.ApplicationClass;
import play.classloading.enhancers.Enhancer;
import play.db.jpa.JPAEnhancer;
import play.db.jpa.JPAPlugin;
import play.libs.WS;

public class LoaderMain {
	public static void main(String[] args) throws ClassNotFoundException, SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, InstantiationException  {
		System.out.println(System.getProperties());
		File root = new File("/Users/hqdvista/Documents/hklab/project/source/iter_1/webservice");
		Play.init(root, System.getProperty("play.id", ""));
		Thread.currentThread().setContextClassLoader(Play.classloader);
		
		 
		 Play.pluginCollection.onApplicationStart();
		 Play.pluginCollection.afterApplicationStart();
		Class<?> c = Play.classloader
				.loadClass("backend.info.PlayLoaderMain");;
		Method m =  c.getMethod("run");
		m.invoke(c.newInstance());
		System.out.println("out");
		Play.stop();
	}
}
