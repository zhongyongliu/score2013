package backend.loader;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import models.Market;
import models.NewsKeyword;
import models.User;

import org.hibernate.annotations.Entity;

import com.google.gson.Gson;

import backend.fetch.HKStockFetcherImpl;
import backend.pojos.JGeneral;
import backend.tools.UrlFetcher;

import play.Play;
import play.db.DBPlugin;
import play.db.jpa.JPA;
import play.db.jpa.JPAPlugin;
import play.test.Fixtures;

public class PlayLoaderMain {
	public void run() throws Exception {
		new DBPlugin().onApplicationStart();
		new JPAPlugin().onApplicationStart();

		JPAPlugin.startTx(false);

		JPAPlugin.closeTx(false);
		System.out.println("finished");
	}

	
	

	public static void main(String args[]) throws InterruptedException,
			IOException {
		UrlFetcher.fetchUrl("http://www.baidu.com/");
	}
}