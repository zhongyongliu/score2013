package backend.pojos;


public class JQuery<T>{
	public int count;
	public String created;
	public String lang;
	public T results;

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JQuery [count=");
		builder.append(count);
		builder.append(", created=");
		builder.append(created);
		builder.append(", lang=");
		builder.append(lang);
		builder.append(", results=");
		builder.append(results);
		builder.append("]");
		return builder.toString();
	}

}