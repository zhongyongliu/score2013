package backend.pojos;

public class JGeneral<T extends DataResult> implements General{
	JQuery<T> query;


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JGeneral [query=");
		builder.append(query);
		builder.append("]");
		return builder.toString();
	}
	
	@Override
	public T internal() {
		return query.results;
	}

}
