package backend.pojos;

import java.util.List;

public class JStockResult implements DataResult{
	List<JStock> quote;

	@Override
	public List<JStock> data() {
		return quote;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JResult [quote=");
		builder.append(quote);
		builder.append("]");
		return builder.toString();
	}

}