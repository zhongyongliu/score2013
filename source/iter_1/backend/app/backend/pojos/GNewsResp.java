package backend.pojos;

import java.util.List;

public class GNewsResp implements General{
	List<GNewsResult> clusters;
	int results_per_page;
    int total_number_of_news;
    List<Integer> news_per_month;//: [26, 0, 0, 1, 0, 1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1],
    int result_start_num;//: 1,
    int result_end_num;//: 10,
    int result_total_articles;//: 33
	@Override
	public List<GNewsResult> internal() {
		return clusters;
	}
}
