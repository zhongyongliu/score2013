package backend.pojos;

import java.util.Date;

import com.gravity.goose.Article;
import com.gravity.goose.Configuration;
import com.gravity.goose.Goose;

import play.Logger;
import play.libs.Codec;

import models.News;
import models.Stock;
import models.StockNews;

public class GNews {

	String t;
	private String u;
	String s;
	String sp;
	String d;
	String tt;
	String usg;
	String sru;
	
	private String code;

	public boolean checkDuplicate()
	{
		String checksum = Codec.hexMD5(u);
		News news = News.find("checksum = ?", checksum).first();
		if (news != null) {
			//already exists
			return true;
		}
		return false;
	}
	public void store(boolean tofile, String url) {
		Article article = null;
		try {
			article = goose.extractContent(getU());
		} catch (Exception e) {
			play.Logger.error("error in extractin content of %s exception %s", this.toString(),e.toString());
			return;
		}
		String content = article.cleanedArticleText();
		String checksum = Codec.hexMD5(u);
		News news = News.find("checksum = ?", checksum).first();
		if (news != null) {
			//already exists
			return;
		}
		news = new News();
		news.content = content;
		news.title = t;
		news.checksum = checksum;
		System.out.println("saving code %s url %s".format(this.code, this.u));
		news.source = this.getU();
		news.abst = sp;
		news.time = new Date(Integer.parseInt(tt));
		news.save();
		
		Stock stock = Stock.findByCode(this.code);
		if (stock == null) {
			play.Logger.error("error in finding stock %s", this.code);
			return;
		}
		StockNews stockNews = new StockNews();
		stockNews.news = news;
		stockNews.stock = stock;
		stockNews.save();
		
		if(tofile)
		{
			
		}
	}
	
	static Goose goose;
	{
		 Configuration configuration = new Configuration();
		 configuration.setEnableImageFetching(false);
		 goose = new Goose(configuration);
	}
	
	public boolean writeForMining(String directory)
	{
		return true;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getU() {
		return u;
	}

	public void setU(String u) {
		this.u = u;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GNews [t=");
		builder.append(t);
		builder.append(", u=");
		builder.append(u);
		builder.append(", s=");
		builder.append(s);
		builder.append(", sp=");
		builder.append(sp);
		builder.append(", d=");
		builder.append(d);
		builder.append(", tt=");
		builder.append(tt);
		builder.append(", usg=");
		builder.append(usg);
		builder.append(", sru=");
		builder.append(sru);
		builder.append(", code=");
		builder.append(code);
		builder.append("]");
		return builder.toString();
	}
	
}
