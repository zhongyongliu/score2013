package backend.pojos;

import java.util.List;

public class GNewsResult implements DataResult{
	public int idx;
	public String id;
	public String lead_story_url;
	public List<GNews> a;
	public String lead_story_doc_id;
	@Override
	public List<GNews> data() {
		return a;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GNewsResult [idx=");
		builder.append(idx);
		builder.append(", id=");
		builder.append(id);
		builder.append(", lead_story_url=");
		builder.append(lead_story_url);
		builder.append(", a=");
		builder.append(a);
		builder.append(", lead_story_doc_id=");
		builder.append(lead_story_doc_id);
		builder.append("]");
		return builder.toString();
	}
}
