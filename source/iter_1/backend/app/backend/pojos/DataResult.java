package backend.pojos;

public interface DataResult {

	public <T> T data();
}
