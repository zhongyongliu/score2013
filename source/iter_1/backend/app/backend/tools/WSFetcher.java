package backend.tools;

import play.libs.WS;

public class WSFetcher implements Fetcher {

	@Override
	public String fetch(String url) {
		return WS.url(url).get().getString();
	}

}
