package backend.tools;

import play.Logger;

import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Response;

public class AsyncFetcher implements Fetcher {

	@Override
	public String fetch(String url) {
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			Response response = client.prepareGet(url).execute().get();
			return  response.getResponseBody();
		} catch (Exception e) {
			Logger.error(e, "error in fetching %s", url);
			return null;
		}
	}

}
