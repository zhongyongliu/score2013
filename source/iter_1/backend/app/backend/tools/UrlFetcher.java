package backend.tools;

public class UrlFetcher {
	private static Fetcher fetcher = new WSFetcher();
	
	public static String fetchUrl(String url)
	{
		return fetcher.fetch(url);
	}
	
}
