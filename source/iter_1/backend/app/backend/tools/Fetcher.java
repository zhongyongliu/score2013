package backend.tools;

public interface Fetcher {
	public String fetch(String url);
}
