package backend.fetch;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import play.Logger;
import repo.TypeRepo;
import backend.pojos.GNewsResp;
import backend.pojos.GNewsResult;
import backend.pojos.JGeneral;
import backend.pojos.GNews;
import backend.pojos.JNewsResult;
import backend.pojos.JStock;
import backend.pojos.JStockResult;
import backend.tools.UrlFetcher;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class HKStockFetcherImpl implements StockFetcher{
	
	private static final String yahoo_stock = "http://query.yahooapis.com/v1/public/yql?format=json&env=http://datatables.org/alltables.env&q=select * from yahoo.finance.quotes where symbol = ";
	private final static String STARTUP_BOARD_URL = "http://www.hkex.com.hk/chi/market/sec_tradinfo/stockcode/eisdgems_pf_c.htm";
	private final static String MARKET_BOARD_URL = "http://www.hkex.com.hk/chi/market/sec_tradinfo/stockcode/eisdeqty_pf_c.htm";
	private final static String YAHOO_NEWS_URL_META = "http://query.yahooapis.com/v1/public/yql?q=select%20title%2C%20link%2C%20description%2C%20pubDate%20from%20rss%20where%20url%3D'http%3A%2F%2Ffinance.yahoo.com%2Frss%2Fheadline%3Fs%3D####'&format=json&callback=";
	private final static String GOOG_NEWS_URL = "http://www.google.com/finance/company_news?q=%s&output=json";
	private static List<String> parseHtml(String url) 
	{
		String html = UrlFetcher.fetchUrl(url);
		Document document = Jsoup.parse(html);
		Element tbody = document.getElementsByClass("table_grey_border").get(0).child(0);
		List<String> codes = new ArrayList<String>();
		for (int i = 1; i < tbody.children().size(); i++) {
			String parcode = (tbody.child(i).child(0).text());
			if (parcode.charAt(0) == '0') {
				parcode = parcode.substring(1);
			}
			codes.add(parcode+".HK");
		}
		return codes;
	}
	
	Gson gson = new Gson();
	
	@Override
	public List<JStock> getStockData(List<String> stockcodes) {
		
		Logger.info("fetched %d symbols", stockcodes.size());
		ArrayList<JStock> ret = new ArrayList<JStock>();
		int count = stockcodes.size(), step = 100, fre = count / step;
		
		for (int i = 0; i < fre; i++) {
			Logger.info("in batch %d", i);
			StringBuilder sb = new StringBuilder();
			sb.append("\"");
			for (int j = 0; j < step; j++) {
				sb.append(stockcodes.get(i * step + j));
				sb.append(",");
			}
			sb.append("\"");
			String url = yahoo_stock + sb.toString();
			String result = UrlFetcher.fetchUrl(url);
			JGeneral<JStockResult> general = null;
			try {
				general = gson.fromJson(result, TypeRepo.stockType);
				ret.addAll(general.internal().data());
			} catch (JsonSyntaxException e) {
				Logger.error(e,"error in parsing Json, batch %d Json content", i,result);
			}
		}
		Logger.info("final ones");
		StringBuilder sb = new StringBuilder();
		sb.append("\"");
		for (int i = fre * step; i < count; i++) {
			sb.append(stockcodes.get(i));
			sb.append(",");
		}
		sb.append("\"");
		String url = yahoo_stock + sb.toString();
		String result = UrlFetcher.fetchUrl(url);
		JGeneral<JStockResult> general = null;
		try {
			general = gson.fromJson(result, TypeRepo.stockType);
			ret.addAll(general.internal().data());
		} catch (JsonSyntaxException e) {
			Logger.error(e,"error in parsing Json, batch final Json content",result);
		}
		return ret;
	}
	
	//public static A
	public static void main(String []args) throws InterruptedException, ExecutionException, IOException
	{
	}


	@Override
	public List<String> getStockCodes() {
		List<String> codes = new ArrayList<String>();
		codes.addAll(parseHtml(STARTUP_BOARD_URL));
		codes.addAll(parseHtml(MARKET_BOARD_URL));
		return codes;
	}

	@Override
	public List<GNews> getStockNews(List<String> stockcodes) {
		List<GNews> news = new ArrayList<GNews>();
		for (int i = 0; i < stockcodes.size(); i++) {
			String code = stockcodes.get(i);
			System.out.println(code);
			//convert from 0066.HK to HKG:0066
			String realcode = "HKG:" + code.substring(0,code.length()-3);
			String json = UrlFetcher.fetchUrl(String.format(GOOG_NEWS_URL, realcode));
			GNewsResp result = null;
			try {
				result = gson.fromJson(json, TypeRepo.newsType);
				//System.out.println(json);
			} catch (JsonSyntaxException e) {
				Logger.error(e,"error in parsing Json, batch final Json content",json);
				throw e;
			}
			if (result != null) {
				List<GNewsResult> target = result.internal();
				//may be null here, {results_per_page:10}
				if (target == null) {
					continue;
				}
				for (int j = 0; j < target.size(); j++) {
					GNewsResult tmp = target.get(j);
					List<GNews> real = tmp.data();
					if (real == null) {
						continue;
					}
					for (int z = 0; z < real.size(); z++) {
						/*
						 * idx: 7,
        id: "2102172",
        tt: "1299140340",
        lead_story_url: "/finance/company_news?q=HKG:0066\x26newsbefore=2011-03-04",
        a: [{
            t: "MTR Corporation Limited Recommends Final Dividend",
            u: "/finance/company_news?q=HKG:0066\x26newsbefore=2011-03-04",
            s: "Reuters Key Development",
            sp: "",
            d: "Mar 3, 2011",
            tt: "1299140340"
        }],
        score: 18
						 */
						//consider this to be invalid
						if (!real.get(z).getU().startsWith("http://") || real.get(z).checkDuplicate()) {
							continue;
						}
						real.get(z).setCode(code);
						news.add(real.get(z));
					}
				}
			}
		}
		return news;
	}
}
