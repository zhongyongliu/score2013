package backend.fetch;

import java.util.List;

import backend.pojos.GNews;
import backend.pojos.JStock;

public interface StockFetcher {

	public List<JStock> getStockData(List<String> codes);
	
	public List<String> getStockCodes();
	
	public List<GNews> getStockNews(List<String> codes);
}

