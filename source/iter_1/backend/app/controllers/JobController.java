package controllers;

import jobs.ClearStockDataJob;
import jobs.FetchNewsInfoJob;
import jobs.FetchStockInfoJob;
import play.mvc.*;

public class JobController extends Controller {

    public static void stockinfo() {
        new FetchStockInfoJob().now();
    }
    
    public static void clearstockdata()
    {
    	new ClearStockDataJob().now();
    }
    
    public static void fetchnews()
    {
    	new FetchNewsInfoJob().now();
    }
}
