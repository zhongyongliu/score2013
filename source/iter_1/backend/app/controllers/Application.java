package controllers;

import backend.pojos.JGeneral;
import backend.pojos.JStock;
import backend.pojos.JStockResult;
import backend.tools.UrlFetcher;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import play.Logger;
import play.mvc.Controller;
import repo.TypeRepo;

public class Application extends Controller {

    public static void index() {
    	/*
    	JWNL.initialize(new FileInputStream("file_properties.xml"));
        WVTool wvt = new WVTool(false);
        WVTConfiguration config = new WVTConfiguration();
        //WVTStemmer stemmer = new WordNetHypernymStemmer();
        //NOTE: wordnet used here!
        WVTStemmer stemmer = new WordNetSynonymStemmer();
        
        config.setConfigurationRule(WVTConfiguration.STEP_STEMMER, new WVTConfigurationFact(stemmer));
        WVTFileInputList list = new WVTFileInputList(0);
        list.addEntry(new WVTDocumentInfo(forWordsStorageDirectoryName, "txt", "", "english")); 
        config.setConfigurationRule(WVTConfiguration.STEP_WORDFILTER, new WVTConfigurationFact(new StopWordsWrapper(2)));
        WVTWordList wordList = wvt.createWordList(list, config);
        //wvt.createWordList(input, config, initialWords, addWords)
        //WVTWordList wordList2 = new WVTWordList(new FileReader(""));
        File dir = new File(forWordsStorageDirectoryName);
//        if(dir.list().length>20)
//        	wordList.pruneByFrequency(1, dir.list().length/10);
//        else
//        	wordList.pruneByFrequency(1, 2);
        if(dir.list().length>8)
        	wordList.pruneByFrequency(1, dir.list().length/2);
        else
        	wordList.pruneByFrequency(1, 4);
        
        File forResultStorageDirectory = new File(forResultStorageDirectoryName);
        forResultStorageDirectory.mkdirs();

        
        wordList.storePlain(new FileWriter(forResultStorageDirectoryName+"/"+"wordlist.txt"));
        wordList.store(new FileWriter(forResultStorageDirectoryName+"/"+"wordlist2.txt"));
        FileWriter outFile = new FileWriter(forResultStorageDirectoryName+"/"+"wv.txt");
        WordVectorWriter wvw = new WordVectorWriter(outFile, true);
        config.setConfigurationRule(WVTConfiguration.STEP_OUTPUT, new WVTConfigurationFact(wvw));
        config.setConfigurationRule(WVTConfiguration.STEP_VECTOR_CREATION, new WVTConfigurationFact(new TFIDF()));
        //config.setConfigurationRule(WVTConfiguration.STEP_TOKENIZER, new WVTConfigurationFact(new NGramTokenizer(20,new SimpleTokenizer())));
        
        wvt.createVectors(list, config, wordList);
        
        
        wvw.close();
        outFile.close();
        */
    	
        
    }

}