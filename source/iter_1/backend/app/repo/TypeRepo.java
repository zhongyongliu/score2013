package repo;

import java.lang.reflect.Type;

import backend.pojos.GNewsResp;
import backend.pojos.JGeneral;
import backend.pojos.JNewsResult;
import backend.pojos.JStockResult;

import com.google.gson.reflect.TypeToken;

public class TypeRepo {

	public final static Type stockType = new TypeToken<JGeneral<JStockResult>>(){}.getType();
	public final static Type newsType = new TypeToken<GNewsResp>(){}.getType();
}
