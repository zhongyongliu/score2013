import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import play.test.UnitTest;
import repo.TypeRepo;
import backend.fetch.HKStockFetcherImpl;
import backend.fetch.StockFetcher;
import backend.pojos.JGeneral;
import backend.pojos.JStock;
import backend.pojos.JStockResult;
import backend.tools.UrlFetcher;

import com.google.gson.Gson;


public class StockFetchTest extends UnitTest{
	Gson gson = new Gson();
	@Test
	public void testJson()
	{
    	String url = "http://query.yahooapis.com/v1/public/yql?format=json&env=http://datatables.org/alltables.env&q=select * from yahoo.finance.quotes where symbol = \"3899.HK,3890.HK\"";
		String result = UrlFetcher.fetchUrl(url);
		JGeneral<JStockResult> general = gson.fromJson(result, TypeRepo.stockType);
	}
	
	@Test
	public void testFetchStock()
	{
		StockFetcher fetcher = new HKStockFetcherImpl();
		List<String> codes = new ArrayList<String>();
		codes.add("0066.HK");
		codes.add("3899.HK");
		/*List<String> codes = fetcher.getStockCodes();
		assertTrue(codes.size() != 0);
		*/
		
		
		List<JStock> stocks = fetcher.getStockData(codes);
		assertTrue(stocks.size() == codes.size());
		
		
	}

}
