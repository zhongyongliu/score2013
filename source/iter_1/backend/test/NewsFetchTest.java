import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import backend.fetch.HKStockFetcherImpl;
import backend.fetch.StockFetcher;
import backend.pojos.GNews;
import backend.pojos.JStock;

import com.google.gson.Gson;

import play.test.UnitTest;


public class NewsFetchTest extends UnitTest {
	Gson gson = new Gson();
	@Test
	public void testFetchNews()
	{
		StockFetcher fetcher = new HKStockFetcherImpl();
		List<String> codes = new ArrayList<String>();
		codes.add("8085.HK");
		codes.add("8082.HK");
		/*List<String> codes = fetcher.getStockCodes();
		assertTrue(codes.size() != 0);
		*/
		
		List<GNews> news = fetcher.getStockNews(codes);
		assertTrue(news.size() != 0);
		
		for (GNews gNews : news) {
			gNews.store(false,"");
		}
		
	}
}
