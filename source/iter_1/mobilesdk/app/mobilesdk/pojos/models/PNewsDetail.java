package mobilesdk.pojos.models;

import java.util.List;


public class PNewsDetail{
	public String content;
	public Integer priority;
	public String note;
	public Float rateindex;
	public String source;
	public List<String> keywords;
}
