package mobilesdk.pojos.models;

public enum PStatus {
	AlreadyExist,
	AuthError,
	ServerError,
	Success,
	InputParamError,
	NotSpecified,
	NotFound
}
