package mobilesdk.pojos.models;

import java.util.List;


public class PStockSearchResult {

	public String stockcode;
	public String stockname;
	public List<String> labels;
	public PStockSearchResult(String stockcode, String stockname) {
		this.stockcode = stockcode;
		this.stockname = stockname;
	}
}
