package mobilesdk.pojos.models;

public class PResult<T> {
	private PStatus status;
	private T result;
	private String reason;
	public PStatus getStatus() {
		return status;
	}
	public void setStatus(PStatus status) {
		this.status = status;
	}
	public T getResult() {
		return result;
	}
	public void setResult(T result) {
		this.result = result;
	}
	public PResult(PStatus status, T result) {
		this.status = status;
		this.result = result;
	}
	
	public PResult(PStatus status, T result,String reason) {
		this.status = status;
		this.result = result;
		this.reason = reason;
	}
	
	public PResult()
	{
		this.status = PStatus.NotSpecified;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
}
