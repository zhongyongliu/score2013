package mobilesdk.pojos.models;

import java.util.Date;


import annotations.Detail;

public class PNews {
	public String abst;
	public Date time;
	public Long id;
	@Detail
	public PNewsDetail detail = null;
	
}
