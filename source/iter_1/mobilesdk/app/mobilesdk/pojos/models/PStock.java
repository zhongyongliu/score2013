package mobilesdk.pojos.models;

import java.io.Serializable;
import java.util.List;


import annotations.Detail;

public class PStock implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8843508647901961575L;
	public String market;
    public String name;
    public String parcode;
    public String fullcode;
    
    
    
    public Float changerate;
    public Float changeamount;
    public Float tradingvolume;
    public Float daylow;
    public Float dayhigh;
    public Float openprice;
    public Float prevclose;
    
    
    public List<Long> newsids;
    
    @Detail
    public PStockDetail detail = null;

}



